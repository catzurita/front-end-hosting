import {Fragment, useState, useEffect, useContext} from 'react';
import { Form, Row, Col} from 'react-bootstrap';
import Link from 'next/link';
import View from '../../components/View';
import UserContext from '../../UserContext';
import Records from '../../components/Records';
import moment from 'moment';
import BarChart from '../../components/BarChart'

export default function index(){
	

	const {user} = useContext(UserContext);
    const [months, setMonths] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
   	const [totalAmount, setTotalAmount] = useState([])
    
    useEffect(() => {

    	setTotalAmount(months.map(month => {
    		let balance = 0;

    		user.records.forEach(record => {

    			if((moment(record.dateAdded)
    				.format('MMMM')) === month){
    				if (record.categoryType == "Income"){
    					balance += parseInt(record.amount)
    				}
    			}
    		})

    		return {
    			balance: balance,
    			month: month
    		}
    	}))
    }, [user])
  	
	return(
		
		<View title={'Monthly Income'}>
			<Fragment>
				<h1>Monthly Income</h1>


				<BarChart monthlyData={totalAmount} />
				
			</Fragment>
		</View>
		
	)
}

