import React, {Fragment,useContext} from 'react';
import Link from 'next/link';

import UserContext from '../UserContext';

export default function Footer(){

	const {user} = useContext(UserContext);

    let style = {
    backgroundColor: "#222222",
    borderTop: "1px solid #222222",
    textAlign: "center",
    padding: "20px",
    position: "bottom",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%",
    color: "white",
    marginTop: "20px"
    }

	
    return (
        <div style={style}>
             <p>
                  <a href="https://www.linkedin.com/in/christian-andrew-zurita-5307a4164/" target="_blank">
                    Budget Tracker
                  </a>
                  &copy; Copyright 2021
            </p>
        </div>
	)

}