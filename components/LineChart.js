import {useState, useEffect} from 'react';
import {colorRandomizer} from '../helpers';
import {Line} from 'react-chartjs-2';

export default function LineChart({categoryData}){
	
	console.log(categoryData)

	const [dates, setDates] = useState([]);
	const [totalBalance, setTotalBalance] = useState([]);
	const [bgColors, setBgColors] = useState([]);



	useEffect(() => {

		if(categoryData.length > 0){

			setDates(categoryData.map(element => element.date))


			setTotalBalance(categoryData.map(element => element.amount))

			setBgColors(categoryData.map(() =>`#${colorRandomizer()}`))
		}

	}, [categoryData])




	const data = {
		labels: dates,
		datasets:[{
			label: 'Balance Trend',
			data:totalBalance,
			fill: false,
			borderColor:bgColors,
			tension:0.1,
			hoverBorderWidth: 2
		}]
	}
	
	return(
		<Line data ={data} />
	)
}