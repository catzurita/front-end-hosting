import {useState, useEffect} from 'react';
import {colorRandomizer} from '../helpers';
import {Pie} from 'react-chartjs-2';

export default function PieChart({categoryData}){
	


	const [categoryName, setCategoryName] = useState([]);
	const [totalBalance, setTotalBalance] = useState([]);
	const [bgColors, setBgColors] = useState([]);



	useEffect(() => {

		if(categoryData.length > 0){

			setCategoryName(categoryData.map(element => element.categoryName))


			setTotalBalance(categoryData.map(element => element.amount))

			setBgColors(categoryData.map(() =>`#${colorRandomizer()}`))
		}

	}, [categoryData])




	const data = {
		labels: categoryName,
		datasets:[{
			data:totalBalance,
			backgroundColor:bgColors,
			hoverBackgroundColor:bgColors
		}]
	}
	
	return(
		<Pie data ={data} />
	)
}